import requests
from .keys import PEXELS_API_KEY, WEATHER_KEY


def get_picture_url(query):
    """
    Get the URL of a picture from the Pexels API.
    """
    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {
        "Authorization": PEXELS_API_KEY
    }
    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": WEATHER_KEY
    }
    response = requests.get(url, params=params)
    api_dict = response.json()
    try:
        latitude = api_dict[0]["lat"]
        longitude = api_dict[0]["lon"]
    except (KeyError, IndexError):
        return None
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": WEATHER_KEY,
        "units": "imperial",
        }

    url = "http://api.openweathermap.org/data/2.5/weather"

    response = requests.get(url, params=params)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],
        "temperature": content["main"]["temp"],
    }
